import { useState } from "react"
import { connect } from "react-redux"
import { add, div, mul, sub, result, reset, handleInput } from "../../redux"

const Calculator = (props) => {
    const [num, setNum] = useState(props.inputNum)
    const handleInputNum = e => {
        setNum(e.target.value)
        props.handleInputDisplay(num)
    } 
    return (
        <div>
            <h4>Calculator</h4>
            <input 
                readOnly
                // value = { props.currentValue }
                value = { props.calculatedValue }
            />
            <input 
                type='text'
                value={ num }
                onChange={ handleInputNum  }
            />
            <button onClick={ () => props.add(num) } >+</button>
            <button onClick={ () => props.sub(num) } >-</button>
            <button onClick={ () => props.mul(num) } >x</button>
            <button onClick={ () => props.div(num) } >/</button>
            <button onClick={ () => props.result() } >=</button>
            <button onClick={ () => { setNum(0); props.clear() }  }>AC</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        inputNum: state.calculator.inputNum,
        currentValue: state.calculator.value,
        calculatedValue: state.calculator.finalValue
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: num => dispatch(add(num)),
        sub: num => dispatch(sub(num)),
        mul: num => dispatch(mul(num)),
        div: num => dispatch(div(num)),
        clear: () => dispatch(reset()),
        result: () => dispatch(result()),
        handleInputDisplay: ev => dispatch(handleInput(ev))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Calculator)