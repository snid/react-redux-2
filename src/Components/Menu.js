import { Paper } from "@material-ui/core"
import { Link, NavLink } from "react-router-dom"
import ReactJS from "./ReactJS"
import ReactRedux from "./ReactRedux"
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(1),
            width: theme.spacing(16),
            height: theme.spacing(16),
        },
    },
}));

const Menu = () => {
    const classes = useStyles();

    return (
        <>
            <div className={classes.root}>
                <Paper elevation={4} />
                <Paper />
                <Paper elevation={3} />
            </div>
            {/* <div className=' container ' className={classes.root}>
                <div className='row row_vcenter'>
                    <div className='col col_vcenter '>
                        <NavLink exact to='/react' className='' activeClassName='' >
                            <Paper elevation={3} square className='bg-warning' >
                                <h2>javascript</h2>
                            </Paper>
                        </NavLink>
                    </div>
                    <div className='col col_vcenter '>
                        <NavLink exact to='/react' className='' activeClassName='' >
                            <Paper elevation={3} square className='bg-success' >
                                <h2>react</h2>
                            </Paper>
                        </NavLink>
                    </div>
                    <div className='col col_vcenter ' >
                        <NavLink exact to='/react-redux' className='' activeClassName='' >
                            <Paper elevation={3} square className='bg-info' >
                                <h2>react-redux</h2>
                            </Paper>
                        </NavLink>
                    </div>
                </div>
            </div> */}
        </>
    )
}

export default Menu