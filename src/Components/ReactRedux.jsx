import { Link, NavLink } from "react-router-dom"

const ReactRedux = () => {
    return (
        <div>            
            <NavLink exact to='/react-redux/counter' >counter</NavLink><br/>
            <NavLink exact to='/react-redux/counterhooks' >counterHooks</NavLink><br/>
            <NavLink exact to='/react-redux/counterbynum' >counterByNum</NavLink><br/>
            <NavLink exact to='/react-redux/calculator' >Basic Calculator</NavLink><br/>
            <NavLink exact to='/react-redux/todo' >ToDo List</NavLink>
            {/* <NavLink exact to='/calculator' >Calculator</NavLink> */}
            {/* REPO LINK + PROJ LINK */}
            {/* EXTERNAL  LINK
            <Link to={{ pathname: "https://www.youtube.com/" }} target="_blank">
                YT
            </Link> */}
        </div>
    )
}

export default ReactRedux