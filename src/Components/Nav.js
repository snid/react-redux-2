import { Link, NavLink } from "react-router-dom"
import ReactJS from "./ReactJS"
import ReactRedux from "./ReactRedux"

const Nav = () => {
    return (
        <div>            
            <NavLink exact to='/react' >react</NavLink><br />
            <NavLink exact to='/react-redux' >react-redux</NavLink><br />
            {/* <NavLink exact to='/counter' >counter</NavLink><br/>
            <NavLink exact to='/counterhooks' >counterHooks</NavLink><br/>
            <NavLink exact to='/counterbynum' >counterByNum</NavLink><br/>
            <NavLink exact to='/calculator' >Basic Calculator</NavLink><br/>
            <NavLink exact to='/todo' >ToDo List</NavLink> */}
            {/* <NavLink exact to='/calculator' >Calculator</NavLink> */}
            {/* REPO LINK + PROJ LINK */}
            {/* EXTERNAL  LINK
            <Link to={{ pathname: "https://www.youtube.com/" }} target="_blank">
                YT
            </Link> */}
        </div>
    )
}

export default Nav