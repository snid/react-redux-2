import { connect } from "react-redux"
import { decrement, increment } from "../../redux"

const Counter = (props) => {
    return (
        <div>
            <h4>Basic Counter</h4>
            <button onClick={ props.decrement }>-</button>
            <input 
                readOnly
                type='text'
                value={ props.value }
            />
            <button onClick={ props.increment }>+</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        value: state.counter.value
    }
}

const mapDispatchToProps = dispatch => {
    return ({
        increment: () => dispatch(increment()),
        decrement: () => dispatch(decrement())
    })
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
) (Counter)