import { useDispatch, useSelector } from "react-redux"
import { incr, decr } from '../../redux'

const CounterHooks = () => {
    const valueHook = useSelector(state => state.counter.valueHook) 
    const dispatch = useDispatch()
    return (
        <div>
            <h4>Counter Using Hooks</h4>
            <button onClick={ () => dispatch( decr() ) }>-</button>
            <input 
                readOnly
                type='text'
                value={ valueHook }
            />
            <button onClick={ () => dispatch( incr() ) }>+</button>
        </div>
    )
}

export default CounterHooks