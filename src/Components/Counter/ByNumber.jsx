import { useState } from "react"
import { connect } from "react-redux"
import { incrByNumber, decrByNumber } from "../../redux"

const ByNumber = (props) => {
    const [num, setNum] = useState(0)
    return (
        <div>
            <h4>Counter By Num</h4>
            <h4>Number: { props.valueByNum } </h4>
            <button onClick={ () =>  { console.log(typeof(props.valueByNum)); props.decrByNum(num) } }>-</button>
            <input
                type='number'
                value={ num }
                onChange = { e => setNum(e.target.value) }
            />
            <button onClick={ () => { console.log(typeof(props.valueByNum)); props.incrByNum(num); } }>+</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        valueByNum: state.counter.valueByNum
    }
}

const mapDispatchToProps = dispatch => {
    return {
        incrByNum: num => dispatch(incrByNumber(num)),
        decrByNum: num => dispatch(decrByNumber(num))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ByNumber)