const List = (props) => {
    return (
        <div key={props.id} >
            <h4>{props.item}
                <span style={{ textAlign: 'right' }}>
                    <button onClick={ () => props.deleteItem(props.id) }>-</button>
                </span>
            </h4>
        </div>
        // <li key={ props.id }>
        //     { props.item}
        // </li>
    )
}

export default List