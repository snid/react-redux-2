import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addItem, clearList, removeItem } from "../../redux"
import List from "./List"

const ToDo = () => {
    const [listItem, setListItem] = useState('')
    const list = useSelector(state => state.todo.list)
    const dispatch = useDispatch()
    // console.log('list',list)
    return (
        <div>
            <h4>ToDo</h4>
            <div>
                <input
                    type='text'
                    value={ listItem }
                    onChange={ e => setListItem(e.target.value) }
                    onKeyPress={ e => e.key === 'Enter' &&  (dispatch(addItem(listItem)) && setListItem('')) }
                />
                <button onClick={ () => { dispatch(addItem(listItem));  setListItem('') } } >+</button>
            </div>
            <div>
                {
                    list
                    ?
                        list.map(item => <List key={item.id} id={item.id} item={ item.content } deleteItem={ () => dispatch(removeItem(item.id))} /> )
                        // list.map(item => console.log('listItem',item))
                    :''
                }
            </div>
            <div>
                <button onClick={ () => dispatch(clearList()) }>Clear</button>
            </div>
        </div >
    )
}

export default ToDo