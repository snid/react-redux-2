import './App.css';
import Nav from './Components/Nav';
import Menu from './Components/Menu';
import Counter from './Components/Counter/Counter';
import CounterHooks from './Components/Counter/CounterHooks';
import ByNumber from './Components/Counter/ByNumber';
import Calculator from './Components/Calculator/Calculator';
import ToDo from './Components/ToDo/ToDo';
import ReactJS from './Components/ReactJS';
import ReactRedux from './Components/ReactRedux';
import { Redirect, Route, Switch } from 'react-router';

function App() {
  return (
    <div className="App">
      {/* <Nav /> */}
      {/* <Menu /> */}
      <Switch>
        <Route exact path='/' component={Menu}  />
        <Route exact path='/react' component={ReactJS} />
        <Route exact path='/react-redux' component={ReactRedux} />
        <Route exact path='/react-redux/counter' component={Counter} />
        <Route exact path='/react-redux/counterhooks' component={CounterHooks} />
        <Route exact path='/react-redux/counterbynum' component={ByNumber} />
        <Route exact path='/react-redux/calculator' component={Calculator} />
        <Route exact path='/react-redux/todo' component={ToDo} />
        <Redirect to='/' component={Menu}  />
      </Switch>
    </div>
  );
}

export default App;
