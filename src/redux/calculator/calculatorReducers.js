import { operations } from './calculatorTypes'

const initialState = {
    initValue: 0,
    value: 0,    
    finalValue: 0,
    strValue: '',
    inputNum: ''
}

const calculatorReducer = (state = initialState, actions) => {
    switch(actions.type){
        case operations.ADD:
            return {
                ...state,
                value: parseFloat(state.value) + parseFloat(actions.payload),
                strValue: state.value + actions.payload + '+',
                finalValue: parseFloat(state.value) + parseFloat(actions.payload)
            }
        case operations.SUB:
            return {
                ...state,
                value: state.value - actions.payload
            }
        case operations.MUL:
            return {
                ...state,
                value: state.value * actions.payload
            }
        case operations.DIV:
            return {
                ...state,
                value: state.value / actions.payload
            }
        case operations.RESULT:
            return {
                ...state,
                finalValue: state.finalValue
            }
        case operations.RESET:
            return {
                ...state,
                value: state.initValue
            }    
        case operations.HANDLE_INPUT:
            return {
                ...state,
                inputNum: state.inputNum + actions.payload  
            }
        default:
            return state
    }
} 

export default calculatorReducer
