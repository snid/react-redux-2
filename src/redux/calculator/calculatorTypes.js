export const operations = {
    ADD: 'ADD',
    SUB: 'SUB',
    MUL: 'MUL',
    DIV: 'DIV',
    RESET: 'RESET',
    RESULT: 'RESULT',
    HANDLE_INPUT: 'HANDLE_INPUT'
}