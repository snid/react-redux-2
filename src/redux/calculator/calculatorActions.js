import { operations } from "./calculatorTypes"

export const add = num => {
    return {
        type: operations.ADD,
        payload: num,
        op: '+'
    }
}
export const sub = num => {
    return {
        type: operations.SUB,
        payload: parseFloat(num)
    }
}
export const mul = num => {
    return {
        type: operations.MUL,
        payload: parseFloat(num)
    }
}
export const div = num => {
    return {
        type: operations.DIV,
        payload: parseFloat(num)
    }
}

export const result = () => {
    return {
        type: operations.RESULT
    }
}

export const reset = () => {
    return {
        type: operations.RESET,
    }
}
export const handleInput = ev => {
    return {
        type: operations.HANDLE_INPUT,
        payload: ev
    }
}


