import { combineReducers } from "redux";
import calculatorReducer from "./calculator/calculatorReducers";
import counterReducer from "./counter/counterReducer";
import todoReducer from "./todo/todoReducers";

const rootReducer = combineReducers({
    counter: counterReducer,
    calculator: calculatorReducer,
    todo: todoReducer,
})

export default rootReducer