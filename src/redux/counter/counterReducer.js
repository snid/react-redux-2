import { counterByNumber, counterHooks, DECREMENT, INCREMENT } from "./counterTypes"

const initialState = {
    value: 1,
    valueHook: 1,
    valueByNum: 0
}

const counterReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case INCREMENT:
            return {
                ...state,
                value: state.value + 1
            }
        case DECREMENT:
            return {
                ...state,
                value: state.value - 1
            }
        case counterHooks.INCR:
            return {
                ...state,
                valueHook: state.valueHook + 1
            }
        case counterHooks.DECR:
            return {
                ...state,
                valueHook: state.valueHook - 1
            }
        case counterByNumber.INCR_BY_NUM:
            return {
                ...state,
                valueByNum: state.valueByNum + actions.payload
            }
        case counterByNumber.DECR_BY_NUM:
            return {
                ...state,
                valueByNum: state.valueByNum - actions.payload
            }
        default:
            return state
    }
}

export default counterReducer