import { DECREMENT, INCREMENT, counterHooks, counterByNumber } from "./counterTypes"

export const increment = () => {
    return {
        type: INCREMENT
    }
}

export const decrement = () => {
    return {
        type: DECREMENT
    }
}

export const incr = () => {
    return {
        type: counterHooks.INCR
    }
}

export const decr = () => {
    return {
        type: counterHooks.DECR
    }
}

export const incrByNumber = incrValue => {
    return {
        type: counterByNumber.INCR_BY_NUM,
        payload: parseFloat(incrValue) 
    }
}

export const decrByNumber = decrValue => {
    return {
        type: counterByNumber.DECR_BY_NUM,
        payload: parseFloat(decrValue)
    }
}

