export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'

export const counterHooks = {
    INCR : 'INCR',
    DECR : 'DECR'
}

export const counterByNumber = {
    INCR_BY_NUM: 'INCR_BY_NUM',
    DECR_BY_NUM: 'DECR_BY_NUM',
}
