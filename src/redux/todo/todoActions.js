import { listActionTypes } from "./todoTypes"

export const addItem = content => {
    return {
        type: listActionTypes.ADD_ITEM,
        payload: {
            id: new Date().getTime().toString(),
            content
        }
    }
}
export const editItem = () => {
    return {
        type: listActionTypes.EDIT_ITEM,
    }
}
export const removeItem = id => {
    return {
        type: listActionTypes.REMOVE_ITEM,
        id
    }
}
export const clearList = () => {
    return {
        type: listActionTypes.CLEAR_LIST
    }
}