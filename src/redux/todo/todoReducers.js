import { listActionTypes } from "./todoTypes"

const initialState = {
    list: [],
}

const todoReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case listActionTypes.ADD_ITEM:
            console.log('actions.payload', actions.payload)
            const { id, content } = actions.payload
            if (content) {
                return {
                    ...state,
                    list: [
                        ...state.list,
                        {
                            id: id,
                            content: content
                        }
                    ]
                }
            }

        case listActionTypes.REMOVE_ITEM:
            const newList = state.list.filter(item => item.id !== actions.id)
            return {
                ...state,
                list: newList
            }
        case listActionTypes.CLEAR_LIST:
            return {
                ...state,
                list: []
            }
        default:
            return state
    }
}

export default todoReducer