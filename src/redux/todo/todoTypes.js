export const listActionTypes = {
    ADD_ITEM: 'ADD_ITEM',
    EDIT_ITEM: 'EDIT_ITEM',
    REMOVE_ITEM: 'REMOVE_ITEM',
    CLEAR_LIST: 'CLEAR_LIST'
}

